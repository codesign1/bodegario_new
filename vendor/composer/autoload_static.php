<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit19377e57098ca5d52243ace21a5e5440
{
    public static $prefixLengthsPsr4 = array (
        'b' => 
        array (
            'bodegario\\' => 10,
        ),
        'V' => 
        array (
            'Velocity\\' => 9,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'bodegario\\' => 
        array (
            0 => __DIR__ . '/../..' . '/',
        ),
        'Velocity\\' => 
        array (
            0 => __DIR__ . '/..' . '/codesign/velocity/src',
        ),
    );

    public static $prefixesPsr0 = array (
        'T' => 
        array (
            'Twig_' => 
            array (
                0 => __DIR__ . '/..' . '/twig/twig/lib',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit19377e57098ca5d52243ace21a5e5440::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit19377e57098ca5d52243ace21a5e5440::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInit19377e57098ca5d52243ace21a5e5440::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
