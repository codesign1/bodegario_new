$("#uploadForm").on('submit',(function(e){
	e.preventDefault();
	$.ajax({
	url: "updateUserImg.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$('#userimg').html(data);
		$('#changeimgcont').fadeToggle('slow');
	},
	error: function(){} 	        
	});
}));
