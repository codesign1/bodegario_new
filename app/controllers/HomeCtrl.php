<?php

namespace bodegario\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Ecommerce\CartController;
use Velocity\Helpers\Helpers;

class HomeCtrl extends CartController {

	public  $main_slider,	
			$productos,
		    $time,
		    $cities,
		    $is_colombia,
		    $img_ejemplo,
		    $productos_featured,
		    $modelos;

	public function init() {
		$this->time = date('H:i');
		$this->cities = array('Bogota', 'Medellin', 'Cali');
		$this->is_colombia = true;
		for ($i=1; $i <= 3; $i++) { 
			$this->main_slider[] = "/public/img/home$i.png";
		}
		$this->img_ejemplo = Helpers::img('BP-04-1.jpg');
		$this->productos_featured = $this->shop->get_productos_featured(8);
		foreach ($this->productos_featured as $key) {
			$modelos = $this->shop->get_modelos_sku($key->sku);
			foreach ($modelos as $modelo) {
				$this->modelos[$modelo->sku][] = array(
					'id' => $modelo->id,
					'sku' => $modelo->sku,
					'color1' => $modelo->color1,
					'color2' => $modelo->color2,
					'img' => $modelo->img
				);
			}
		}
	}

	public function by_id($id) {
		$this->id = $id;
		$this->db_name = Config::get('mysql/host');
	}

	public function multiple_params($name, $hour, $minute) {
		$this->hour_name = $name;
		$this->hour_hr = $hour;
		$this->hour_min = $minute;
	}
}
