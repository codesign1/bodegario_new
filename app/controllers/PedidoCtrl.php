<?php

namespace bodegario\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Helpers\Redirect;
use Velocity\Authentication\Input;
use Velocity\Authentication\Token;
use Velocity\Authentication\Validate;
use Velocity\Ecommerce\CartController;

class PedidoCtrl extends CartController {

	public  $variable,
			$bono,
			$promocion;

	public function init() {
		$this->variable = date('H:i');
		$this->bono = 'bono';
		$this->promocion = 'promo';
		$this->payu_api_key = '4Vj8eK4rloUd272L48hsrarnUA';
	}

	public function hacer_pedido() {

		$this->payu_url = 'https://sandbox.gateway.payulatam.com/ppp-web-gateway';
		$this->merchantId = Input::get('merchantId');
		$this->accountId = Input::get('accountId');
		$this->description = Input::get('description');
		$this->referenceCode = Input::get('referenceCode');
		$this->amount = Input::get('amount');
		$this->tax = Input::get('tax');
		$this->taxReturnBase = Input::get('taxReturnBase');
		$this->currency = Input::get('currency');
		$this->signature = Input::get('signature');
		$this->test = Input::get('test');
		$this->responseUrl = 'http://bodegario.com/respuesta.php';
		$this->confirmationUrl = 'http://bodegario.com/confirmacion';
		$this->shippingCountry = Input::get('shippingCountry');
		$this->shippingCity = Input::get('shippingCity');
		$this->shippingAddress = Input::get('shippingAddress');
		$this->zipCode = Input::get('zipCode');
		$this->buyerFullName = Input::get('buyerFullName');
		$this->payerPhone = Input::get('payerPhone');
		$this->buyerEmail = Input::get('buyerEmail');
		$this->payerMobilePhone = Input::get('payerMobilePhone');
		$this->extra1 = (INPUT::get('extra1')) ? INPUT::get('extra1') : 'None';
		$this->extra2 = (INPUT::get('extra2')) ? INPUT::get('extra2') : 'None';
		$this->envio = floatval(Input::get('envio'));

		if($this->extra1 != 'None') {
			$this->descuento = $this->shop->check_codigo($this->extra1);
		} else {
			$this->descuento = 1;
		}

		if($this->extra2 != 'None') {
			$this->bono = $this->shop->check_bono($this->extra2);
		} else {
			$this->bono = 0;
		}

		$datos = array(
			'nombre' => $this->buyerFullName,
			'email' => $this->buyerEmail,
			'celular' => $this->payerPhone,
			'direccion' => $this->shippingAddress,
			'ciudad' => $this->shippingCity
		);

		$amount_total = $this->shop->hacer_pedido($this->referenceCode, $datos, $this->envio, $this->descuento, $this->bono);
		$this->amount_total = $amount_total;
		$this->signature = md5($this->payu_api_key.'~'.$this->merchantId.'~'.$this->referenceCode.'~'.$amount_total.'~'.$this->currency);

	}

}
