<?php

namespace bodegario\app\controllers;

use Velocity\Config\Config;
use Velocity\Helpers\Helpers;
use Velocity\Core\Controller;
use Velocity\Ecommerce\CartController;
use Velocity\Users\User;
use Velocity\Db\Db;
use Velocity\Ecommerce\Shop;


class ShopCtrl extends CartController {

	public  $time,
			$id,
			$products,
			$modelos,
			$models,
			$categorias,
			$cat,
			$subcat,
			$filtro,
			$filtro_title,
			$page,
			$num_products_per_page,
			$num_pages;

	public function init() {
		$this->num_products_per_page = 36;
		$this->time = date('H:i');
		$this->img_ejemplo = Helpers::img('BP-04-1.jpg');
		$this->get_product('123');
		$this->products = $this->get_products();
		$this->modelos = $this->shop->get_modelos();
		$this->models = array();
		foreach ($this->modelos as $modelo) {
			$this->models[$modelo->sku][] = array(
				'id' => $modelo->id,
				'sku' => $modelo->sku,
				'color1' => $modelo->color1,
				'color2' => $modelo->color2,
				'img' => $modelo->img
			);
		}
		$this->filtro_title = 'Todos';
	}

	public function get_product($sku) {
		$this->id = $sku;
	}

	public function get_products($cat = '', $subcat = '', $filtro = '', $page = 1) {

		$this->cat = ($cat=='') ? 'todos' : $cat;
		$this->subcat = ($subcat=='') ? 'todos' : $subcat;
		$this->filtro = ($filtro=='') ? 'todos' : $filtro;
		$this->page = $page;

		$this->products = array();

		if($cat != '' && $cat != 'todos') {
			$url_cat = str_replace('-', ' ', $cat);
			$this->filtro_title = substr(ucfirst($url_cat), 0, 26);
		}

		$prods = $this->shop->get_prod($this->cat, $this->subcat, $this->filtro);
		$this->num_pages = ceil(count($prods) / $this->num_products_per_page);

		if(count($prods) < $this->num_products_per_page) {
			$last_product = count($prods);
			$first_product = $last_product - count($prods);
		} else {
			$last_product = $page * $this->num_products_per_page;
			$first_product = $last_product - $this->num_products_per_page;
		}

		for ($i=$first_product; $i < $last_product; $i++) { 
			if($prods[$i]) {
				$this->products[] = $prods[$i];
			}
		}

		return $this->products;
	}

	public function organize_modelos() {

	}

}
