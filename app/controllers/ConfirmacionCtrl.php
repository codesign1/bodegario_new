<?php

namespace bodegario\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Ecommerce\CartController;
use Velocity\Authentication\Input;
use Velocity\Email\PHPMailer;

class TemplateCtrl extends CartController {

	public  $variable;

	public function init() {
		$this->variable = date('H:i');
	}

	public function manejar_form() {

		$unique_id = Input::get('reference_sale');

		if(Input::get('state_pol')==4) {
			
			if($this->shop->payed_order($unique_id)) {

				$pedido_master = $this->shop->get_pedido_master($unique_id);

				$message = file_get_contents(dirname(__FILE__).'/../../assets/email/confirmacion.html');
				$message = str_replace('%NOMBRE%', $pedido_master->nombre, $message);
				$message = str_replace('%CURRENT_YEAR%', date('Y'), $message);
				
				$mail = new PHPMailer;                        
				$mail->setFrom('contacto@bodegario.com', 'Equipo Bodegario');
				$mail->addReplyTo('contacto@bodegario.com', 'Equipo Bodegario');
				$mail->addAddress($pedido_master->email, $pedido_master->nombre);
				$mail->isHTML(true);
				$mail->Subject = 'Pedidos Bodegario';
				$mail->msgHTML($message , dirname(__FILE__));
				//send the message, check for errors
				$mail->AltBody = 'Hecha con amor por Estudio Codesign';
				if (!$mail->send()) {
				    $errors[] = 'No se pudo enviar el email a: ' . $email;
				}

			}

		} elseif (Input::get('state_pol')==6) {

			$this->shop->payment_declined($unique_id);

		}

	}
}
