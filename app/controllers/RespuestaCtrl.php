<?php

namespace bodegario\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Authentication\Input;
use Velocity\Helpers\Helpers;
use Velocity\Ecommerce\CartController;

class RespuestaCtrl extends CartController {

	public  $mensaje;

	public function init() {
		$this->mensaje = 'Gracias por tu compra!';
	}

	public function manejar_form($status) {
		if($status!='gracias') {
			$this->mensaje = 'Hubo un error procesando tu compra!';
		} else {
			$this->mensaje = 'Gracias por tu compra!';
		}
	}

}
