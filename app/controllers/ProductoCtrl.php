<?php

namespace bodegario\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Ecommerce\CartController;
use Velocity\Helpers\Helpers;

class ProductoCtrl extends CartController {

	public  $variable,
			$producto,
			$img_ejemplo,
			$modelos,
			$otros_productos,
			$otros_modelos;

	public function init() {
		$this->variable = date('H:i');
		$this->img_ejemplo = Helpers::img('BP-04-1.jpg');
	}

	public function get_product($url) {
		$this->producto = $this->shop->get_product($url, 'url');
		$this->otros_productos = $this->shop->get_productos_relacionados($this->producto->sku);
		$this->modelos = $this->shop->get_modelos_sku($this->producto->sku);

		foreach ($this->otros_productos as $key) {
			$mode = $this->shop->get_modelos_sku($key->sku);
			foreach ($mode as $m) {
				$this->otros_modelos[$m->sku][] = array(
					'id' => $m->id,
					'sku' => $m->sku,
					'color1' => $m->color1,
					'color2' => $m->color2,
					'img' => $m->img
				);
			}
		}
	}
}
