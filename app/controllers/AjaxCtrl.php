<?php

namespace bodegario\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;

class AjaxCtrl extends Controller {

	public  $variable;

	public function init() {
		$this->variable = date('H:i');
	}
}
