<?php

namespace bodegario\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;

class ShopCtrl extends Controller {
	public $time;
	public $cities;
	public $is_colombia;

	public function init() {
		$this->time = date('H:i');
		$this->cities = array('Bogota', 'Medellin', 'Cali');
		$this->is_colombia = true;
	}

	public function by_id($id) {
		$this->id = $id;
		$this->db_name = Config::get('mysql/host');
	}

	public function multiple_params($name, $hour, $minute) {
		$this->hour_name = $name;
		$this->hour_hr = $hour;
		$this->hour_min = $minute;
	}
}
