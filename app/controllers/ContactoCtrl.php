<?php

namespace bodegario\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Authentication\Input;
use Velocity\Authentication\Token;
use Velocity\Helpers\Helpers;

class ContactoCtrl extends Controller {

	public  $variable,
			$nombre,
			$token;

	public function init() {
		$this->variable = date('H:i');
		$this->token = Token::generate();
	}

	public function enviar_mensaje() {
		$this->nombre = $_POST['nombre'];
	}
}
