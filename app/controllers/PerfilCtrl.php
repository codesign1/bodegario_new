<?php

namespace bodegario\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Helpers\Redirect;
use Velocity\Authentication\Input;
use Velocity\Authentication\Token;
use Velocity\Authentication\Validate;
use Velocity\Authentication\Session;
use Velocity\Security\Hash;
use Velocity\Email\PHPMailer;
use Velocity\Ecommerce\CartController;

class PerfilCtrl extends CartController {

	public  $variable,
			$accion,
			$errors,
			$message,
			$opcion;

	public function init() {
		$this->variable = date('H:i');
		$this->accion = 'login';
		$this->token = Token::generate();
		$this->errors = array();
		$this->opcion = 'direcciones';
		if(Session::exists('login')) {
			$this->message = Session::flash('login');
		} else {
			$this->message = '';
		}
	}

	public function accion($accion, $opcion = 'direcciones'){
		$this->accion = $accion;
		$this->opcion = $opcion;
		if($opcion=='logout') {
			$this->user->logout();
			Redirect::to('/shop');
		}
	}

	public function manejar_form() {
		if(Input::get('hacer')=='registro') {
			
			$validate = new Validate();
			$validation = $validate->check($_POST, array(
				'username' => array(
					'required' => true,
					'unique' => 'v_users'
				),
				'password' => array(
					'required' => true,
					'min' => 6
				)
			));
			if($validation->passed()) {

				$username = Input::get('username');
				$password = Input::get('password');
				$salt = Hash::salt(32);
			
				try {

					$this->user->create(array(
						'username' => Input::get('username'),
						'password' => Hash::make(Input::get('password'), $salt),
						'email' => Input::get('username'),
						'salt' => $salt,
						'joined' => date('Y-m-d H:i:s'),
						'group' => 2
					));

					Session::flash('login', 'You have been registered and can now log in!');
					Redirect::to('/perfil');

				} catch(Exception $e) {
					die($e->getMessage());
				}

			} else {
				$this->errors = $validation->errors();
			}
		
		} elseif (Input::get('hacer')=='login') {
			
			$validate = new Validate();
			$validation = $validate->check($_POST, array(
				'username' => array(
					'required' => true
				),
				'password' => array(
					'required' => true
				)
			));
			if($validation->passed()) {

				$login = $this->user->login(Input::get('username'), Input::get('password'), true);

				if($login) {

					
					
					Redirect::to('/perfil');
				} else {
					echo 'Sorry, login failed.';
				}

			} else {
				$this->errors = $validation->errors();
			}

		} elseif (Input::get('hacer')=='password') {

			if($this->user->find(Input::get('email'))) {

				$new_password = Helpers::generateRandomString(20);
				$salt = Hash::salt(32);
				$user_id = $this->user->user_by_email(Input::get('email'))->id;

				try {

					$this->user->update(array(
						'password' => Hash::make($new_password, $salt),
						'salt' => $salt
					), $user_id);

					$mail = new PHPMailer;
					$mail->setFrom('contacto@bodegario.com', 'Contacto Bodegario');
					$mail->addReplyTo('contacto@bodegario.com', 'Contacto Bodegario');
					$mail->addAddress(Input::get('email'), Input::get('email'));
					$mail->isHTML(true);
					$mail->Subject = 'Nueva Contraseña Bodegario';
					$body = '<h1>Nueva Contraseña:</h1>';
					$body .= '<br><p>'.$new_password.'</p>';
					$mail->Body = $body;
					if (!$mail->send()) {
					    $status = $mail->ErrorInfo;
					} else {
					    Session::flash('login', 'Ya te enviamos tu nueva contraseña.');
					}
					
				} catch(Exception $e) {
					die($e->getMessage());
				}

			} else {
				$this->errors[] = 'No existe este email.';
			}

		}
	}

	public function crear_direccion(){
		$this->content = 'informacion';
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			'ciudad' => array(
				'required' => true
			),
			'barrio' => array(
				'required' => true
			),
			'direccion' => array(
				'required' => true
			)
		));
		if($validation->passed()) {

			if($this->shop->crear_direccion(Input::get('ciudad'), Input::get('barrio'), Input::get('direccion'))) {
				Redirect::to('/perfil/mi-perfil/direcciones');
			}

		} else {
			$this->errors = $validation->errors();
		}
	}

}
