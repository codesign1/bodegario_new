<?php

namespace bodegario\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Helpers\Redirect;
use Velocity\Authentication\Input;
use Velocity\Authentication\Token;
use Velocity\Authentication\Validate;
use Velocity\Ecommerce\CartController;

class CheckoutCtrl extends CartController {

	public  $variable,
			$content,
			$modelos_pro,
			$productos,
			$payu_api_key,
			$payu_merchant_id,
			$payu_reference_code,
			$payu_amount,
			$payu_currency,
			$payu_signature,
			$payu_account_id,
			$errors,
			$otros_productos,
			$modelos,
			$otros_modelos;

	public function init() {

		$this->variable = date('H:i');
		$this->content = 'carrito';
		$this->modelos_pro = array();
		$this->productos = array();
		$this->payu_api_key = '4Vj8eK4rloUd272L48hsrarnUA';
		$this->payu_merchant_id = '508029';
		$this->payu_account_id = '512321';
		$this->payu_reference_code = Helpers::generateRandomString(30);
		$this->payu_amount = $this->cart_total;
		$this->payu_currency = 'COP';	
		$this->payu_signature = md5($this->payu_api_key.'~'.$this->payu_merchant_id.'~'.$this->payu_reference_code.'~'.$this->payu_amount.'~'.$this->payu_currency);	
		$this->otros_productos = $this->shop->get_productos_relacionados();

		$this->modelos = $this->shop->get_modelos_sku($this->producto->sku);

		foreach ($this->otros_productos as $key) {
			$mode = $this->shop->get_modelos_sku($key->sku);
			foreach ($mode as $m) {
				$this->otros_modelos[$m->sku][] = array(
					'id' => $m->id,
					'sku' => $m->sku,
					'color1' => $m->color1,
					'color2' => $m->color2,
					'img' => $m->img
				);
			}
		}

		foreach ($this->cart as $item) {
			$pro = $this->shop->get_product($item['sku'], 'sku');
			$this->productos[] = $pro;
			$mo = $this->shop->get_modelos_sku($item['sku'], $item['modelo']);
			$this->modelos_pro[] = $mo[0];
		}

	}

	public function toggle_content($what = 'carrito') {
		$this->content = $what;
	}

	public function crear_direccion(){
		$this->content = 'informacion';
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			'ciudad' => array(
				'required' => true
			),
			'barrio' => array(
				'required' => true
			),
			'direccion' => array(
				'required' => true
			)
		));
		if($validation->passed()) {

			if($this->shop->crear_direccion(Input::get('ciudad'), Input::get('barrio'), Input::get('direccion'))) {
				Redirect::to('/checkout/informacion');
			}

		} else {
			$this->errors = $validation->errors();
		}
	}
}

























