<?php

namespace bodegario\app\controllers;

use Velocity\Config\Config;
use Velocity\Helpers\Helpers;
use Velocity\Core\Controller;
use Velocity\Ecommerce\CartController;
use Velocity\Users\User;
use Velocity\Db\Db;
use Velocity\Ecommerce\Shop;
use Velocity\Cms\Cms;

class TemplateCtrl extends CartController {

	public  $prods,
			$cms,
			$count;

	public function init() {
		$this->prods = $this->shop->get_prod('todos', 'todos', 'todos');
		$this->count = 0;
		$this->cms = new CMS();

		foreach ($this->prods as $pro) {
			if(stripos($pro->descripcion, 'í') !== false) {
				$des = str_replace('í', 'i', $pro->descripcion);
				$this->cms->update('v_productos', $pro->id, array(
					'descripcion' => $des
				));
				$this->count++;
			}
		}

	}
}
