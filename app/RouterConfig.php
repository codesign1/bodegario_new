<?php

namespace bodegario\App;

use Velocity\Core\Router;

class RouterConfig {
	public static function execute() {
		/**
		 * These two routes point to the same resource, i.e., controller when accessed
		 * using a GET request
		 */
		Router::get('/', 'home');
		Router::get('/suscribe', 'suscribe');
		Router::get('/shop', 'shop');
		Router::get('/template', 'template');
		Router::get('/home', 'home');
		Router::get('/producto', 'producto');
		Router::get('/contacto', 'contacto');
		Router::get('/nosotros', 'nosotros');
		Router::get('/checkout', 'checkout');
		Router::get('/perfil', 'perfil');
		Router::post('/pedido', 'pedido#hacer_pedido');
		Router::post('/contacto', 'contacto#enviar_mensaje');
		Router::post('/perfil', 'perfil#manejar_form');
		Router::post('/confirmacion', 'confirmacion#manejar_form');

		/**
		 * This is a route with basic parameters. It uses PHP regular expressions with named
		 * capture groups. The capture group name is the name of the parameter sent to the
		 * by_id method in the time controller
		 */
		Router::get('/respuesta/(?<status>[0-9a-zA-Z-]+)', 'respuesta#manejar_form');
		Router::get('/perfil/(?<accion>[0-9a-zA-Z-]+)', 'perfil#accion');
		Router::get('/time/(?<id>[0-9a-zA-Z-]+)', 'time#by_id');
		Router::get('/checkout/(?<what>[0-9a-zA-Z-]+)', 'checkout#toggle_content');
		Router::post('/checkout/(?<what>[0-9a-zA-Z-]+)', 'checkout#crear_direccion');
		Router::get('/producto/(?<url>[0-9a-zA-Z-]+)', 'producto#get_product');

		/**
		 * When using multiple parameters, they are sent in order of appearance to
		 * the corresponding controller method
		 *
		 * Parameters can be defined to be of a certain type, like text or numbers only
		 */
		Router::get('/perfil/(?<accion>[0-9a-zA-Z-]+)/(?<opcion>[0-9a-zA-Z-]+)', 'perfil#accion');
		Router::post('/perfil/(?<accion>[0-9a-zA-Z-]+)/(?<opcion>[0-9a-zA-Z-]+)', 'perfil#crear_direccion');
		Router::get('/time/(?<name>[A-z]+)/hour/(?<hour>[0-9a-zA-Z-]+)/minute/(?<minute>[0-9a-zA-Z-]+)', 'time#multiple_params');
		Router::get('/shop/(?<cat>[0-9a-zA-Z-]+)', 'shop#get_products');
		Router::get('/shop/(?<cat>[0-9a-zA-Z-]+)/(?<subcat>[0-9a-zA-Z-]+)', 'shop#get_products');
		Router::get('/shop/(?<cat>[0-9a-zA-Z-]+)/(?<subcat>[0-9a-zA-Z-]+)/(?<filtro>[0-9a-zA-Z-]+)', 'shop#get_products');
		Router::get('/shop/(?<cat>[0-9a-zA-Z-]+)/(?<subcat>[0-9a-zA-Z-]+)/(?<filtro>[0-9a-zA-Z-]+)/(?<page>[0-9a-zA-Z-]+)', 'shop#get_products');


	}
}
