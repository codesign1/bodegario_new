<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

## INCLUDE VENDOR
include 'config/configuration.php';
include 'vendor/autoload.php';

## USE NAMESAPCES
use Velocity\Velocity;
use Velocity\Authentication\Input;
use Velocity\Helpers\Helpers;
use Velocity\Ecommerce\Shop;

## EXECUTE CODE
$shop = new Shop();
if(Input::get('url')=='atras') {
	$subcat = $shop->get_categorias('categorias');
	$cat_title = 'todos';
} else {
	$subcat = $shop->get_sub_categorias(Input::get('url'));
	$cat = $shop->get_categoria(Input::get('url'));
	$cat_title = $cat->nombre;
}

?>

<h3 class="text-center"><?php echo ucfirst($cat_title); ?></h3>
<div class="grid-12-xs text-center">
	<span class="text-center marcas">Marcas Exclusivas <i class="icon-star2"></i></span>
</div>
<?php if ($cat_title!='todos') { ?>
<div class="grid-12-xs menucatclickable last menucat" value="atras">
	<div class="cont90">
		<div class="grid-1-xs">
			<i class="icon-arrow-left-thick arrow"></i>
		</div>
		<div class="grid-11-xs last">
			<span>Atras</span>
		</div>
	</div>
</div>
<?php } ?>
<div class="grid-12-xs last seecat menucat" url="<?php echo Helpers::url('shop/'); ?>">
	<div class="cont90"> 
		<div class="grid-11-xs">
			<span>Todos</span>
		</div>
		<div class="grid-1-xs last">
			<i class="icon-arrow-right-thick arrow"></i>
		</div>
	</div>
</div>
<?php foreach ($subcat as $val) { 
	if($cat_title=='todos') {
		$s = $shop->get_sub_categorias($val->url); 
	} ?>
	<div class="grid-12-xs last seecat menucat" url="<?php if($cat_title=='todos') { echo Helpers::url('shop/'.$val->url); } else { echo Helpers::url('shop/'.Input::get('url').'/'.$val->url); } ?>">
		<div class="cont90">
			<div class="grid-11-xs">
			<?php if($val->marca==1) { echo '<i class="icon-star2"></i>'; } ?>
				<span><?php echo $val->nombre; ?></span>
			</div>
			<div class="grid-1-xs last">
				<i class="icon-arrow-right-thick arrow"></i>
			</div>
		</div>
	</div>
<?php } ?>