/**
 * Created by jsrolon on 16-06-2016.
 */

 var page_url = "http://bodegario.com/";

$(document).on('click', '#menumob, .contdir, #cart, #categorias, .addfast, .agregarcart, .pluscant, .minuscant', function(event){
	event.stopPropagation();
});

$(document).on('click', 'html', function(event){
	if($('#cart').hasClass('open')) {
		$('#cart').removeClass('open');
		$('#carticon').addClass('icon-cart');
		$('#carticon').removeClass('icon-times');
		$('#carticon').removeClass('backred');
		$('#carticon').removeClass('text-white');	
		$('#cart').slideToggle();
	}
	if($("#categorias").hasClass('open')) {
		$("#categorias").removeClass('open');
		$('#botoncategorias').html('CATEGORÍAS');
		$('#botoncategorias').addClass('backred');
		$('#botoncategorias').removeClass('backredshadow');
		$( "#categorias" ).slideToggle();
	}
	if($('#creardireccion').hasClass('open')) {
		$('#creardireccion').removeClass('open');
		$('#creardireccion').fadeOut('slow');
	}
	if($('#menumob').hasClass('open')) {
		$('#menumob').removeClass('open');
		$('#menuiconmobile').addClass('icon-th-menu');
		$('#menuiconmobile').removeClass('icon-times');
		$('#menumob').slideToggle('slow');
	}
});

var  tb = $('.headerin');
tbs = "menuup-scrolled";

$(window).scroll(function() {
  	if($(this).scrollTop()) {
    	tb.addClass(tbs);
  	} else {
    	tb.removeClass(tbs);
  	}
});

var images_url = "http://bodegario.com/public/img/";
var images = new Array ('footer1.png', 'footer2.png', 'footer3.png', 'footer4.png', 'footer5.png', 'footer6.png', 'footer7.png', 'footer8.png');
var index = 1;
$('#heart').attr('src', images_url+'footer1.png');
$('#logofooter').attr('src', images_url+'logofooter2.png');
$('#cards').attr('src', images_url+'cards.png');

function rotateImage() {
  	$('#heart').fadeOut('fast', function() {
	    $(this).attr('src', images_url+images[index]);
	    $(this).fadeIn('fast', function() {
	      	if (index == images.length-1) {
	        	index = 0;
	      	} else {
	        	index++;
	      	}
	    });
  	});
}

setInterval (rotateImage, 1500);

var header_height = $('header').height();
var viewportHeight = window.innerHeight;
var viewportWidth = window.innerWidth;
var categorias_height = viewportHeight - header_height;
var cart_height_max = viewportHeight - 200;

if(viewportWidth < 420) {
	$('#cart').css('width', '100%');
}

if(viewportWidth > 768) {
	var categorias_height = viewportHeight - header_height;
	$('#categorias').css('height', categorias_height);
	$('#categorias').css('top', header_height);
	$('#menumob').css('top', 91);
	$('#cart').css('top', header_height);
	$('#cart').css('max-height', cart_height_max);
} else {
	var categorias_height = viewportHeight - 91;
	$('#categorias').css('height', categorias_height);
	$('#categorias').css('top', 91);
	$('#menumob').css('top', 91);
	$('#cart').css('top', 91);
	$('#cart').css('max-height', cart_height_max);
}



$(window).resize(function() {
	var header_height = $('header').height();
	var viewportHeight = window.innerHeight;
	var viewportWidth = window.innerWidth;
	var categorias_height = viewportHeight - header_height;
	var cart_height_max = viewportHeight - 200;

	if(viewportWidth > 768) {
		var categorias_height = viewportHeight - header_height;
		$('#categorias').css('height', categorias_height);
		$('#categorias').css('top', header_height);
		$('#menumob').css('top', 91);
		$('#cart').css('top', header_height);
		$('#cart').css('max-height', cart_height_max);
	} else {
		var categorias_height = viewportHeight - 91;
		$('#categorias').css('height', categorias_height);
		$('#categorias').css('top', 91);
		$('#menumob').css('top', 91);
		$('#cart').css('top', 91);
		$('#cart').css('max-height', cart_height_max);
	}
});

$(document).on('click', '#botoncategorias', function(event){
	event.stopPropagation();
	if($("#categorias").hasClass('open')) {
		$("#categorias").removeClass('open');
		$(this).html('CATEGORÍAS');
		$(this).addClass('backred');
		$(this).removeClass('backredshadow');
	} else {
		$("#categorias").addClass('open');
		$(this).html('<i class="icon-times"></i>CERRAR');
		$(this).addClass('backredshadow');
		$(this).removeClass('backred');		
	}
	$( "#categorias" ).slideToggle();
});

$(document).on('click', '#carticon', function(event){
	event.stopPropagation();
	if($('#cart').hasClass('open')) {
		$('#cart').removeClass('open');
		$(this).addClass('icon-cart');
		$(this).removeClass('icon-times');
		$(this).removeClass('backred');
		$(this).removeClass('text-white');	
	} else {
		$('#cart').addClass('open');
		$(this).addClass('icon-times');
		$(this).removeClass('icon-cart');	
		$(this).addClass('backred');
		$(this).addClass('text-white');		
	}
	$('#cart').slideToggle();
});


$(document).on('click', '#opensearch', function(){
	$('#searchcont').fadeIn();
	$( "#search_input" ).focus();
});

$(document).on('click', '#closesearch', function(){
	$('#searchcont').fadeOut();
});

$(document).on('click', '.menucatclickable', function(){
	var url = $(this).attr('value');
	$.ajax({
	    type:"GET",
	    url: page_url +  "ajax.php",
	    data:"url=" + url,
	    success:function(e) {
	      	$("#categorias").html(e)
	    }
	});
});

$(document).on('click', '.seecat', function(){
	var url = $(this).attr('url');
	location.href = url;
});

$(document).on('click', '.choosecolor', function(){
	var image = "/public/img/" + $(this).attr('img');
	var imageurl = 'url('+image+')';
	var sku = $(this).attr('sku');
	var model_id = $(this).attr('id');
	var image_element = $('#img'+sku);
	var prod_element = $('#prod'+sku);
	image_element.attr('src', image);
	image_element.attr('data-overlay', image);
	image_element.attr('data-big', image);
	image_element.attr('data-big2x', image);
	$('#photospecial').attr('src', image);
	$('#mlens_target_0').css('background-image', imageurl);
	prod_element.attr('model', model_id);
});

$(document).on('click', '.choosecolor', function(){
	var modelo_id = $(this).attr('modelo');
	$('.agregarcart').attr('modelo', modelo_id);
	if($(this).hasClass('coloroutline')) {
		$(this).removeClass('coloroutline');
	} else {
		$(this).parent().children('span').removeClass('coloroutline');
		$(this).addClass('coloroutline');
	}
});

$(document).on('click', '.addfast', function(){
	var sku = $(this).attr('sku');
	var modelo = $(this).attr('model');
	$.ajax({
	    type:"GET",
	    url: page_url +  "cart.php",
	    data:"action=add&sku=" + sku + "&modelo=" + modelo,
	    success:function(e) {
	      	if(!$('#cart').hasClass('open')) {
	      		$('#cart').addClass('open');
				$('#carticon').addClass('icon-times');
				$('#carticon').removeClass('icon-cart');	
				$('#carticon').addClass('backred');
				$('#carticon').addClass('text-white');	
				$('#cart').slideToggle();
	      	}
	      	$('#cart').html(e);
	    }
	});
});
	
var total_carrito = parseInt($('#totalcheckout').attr('precio'));

$(document).on('click', '.pluscant', function(){
	var cantdiv = $(this).parent();
	var cant = parseInt(cantdiv.attr('value'));
	var newcant = cant + 1;
	cantdiv.attr('value', newcant);
	$('.agregarcart').attr('cant', newcant);
	cantdiv.html(newcant+' <i class="icon-plus2 pluscant"></i><i class="icon-minus minuscant"></i>');
});

$(document).on('click', '.pluscantcheckout', function(){
	var sku = $(this).attr('sku');
	var modelo = $(this).attr('modelo');
	var precio = parseInt($(this).attr('precio'));
	var cantdiv = $(this).parent();
	var cant = parseInt(cantdiv.attr('value'));
	var newcant = cant + 1;
	cantdiv.attr('value', newcant);
	$('.agregarcart').attr('cant', newcant);
	cantdiv.html(newcant+' <i class="icon-plus2 pluscantcheckout" precio="'+ precio +'" sku="'+sku+'" modelo="'+modelo+'"></i><i class="icon-minus minuscantcheckout" precio="'+ precio +'" sku="'+sku+'" modelo="'+modelo+'"></i>');
	total_carrito += precio;
	$('#totalcheckout').html('TOTAL: <span class="text-light">$'+addCommas(total_carrito)+'</span>');
	$.ajax({
	    type:"GET",
	    url: page_url +  "cart.php",
	    data:"action=add&sku=" + sku + "&modelo=" + modelo
	});
});

$(document).on('click', '.minuscantcheckout', function(){
	var precio = parseInt($(this).attr('precio'));
	var cantdiv = $(this).parent();
	var cant = cantdiv.attr('value');
	if(cant > 1) {
		var newcant = cant - 1;
		cantdiv.attr('value', newcant);
		$('.agregarcart').attr('cant', newcant);
		cantdiv.html(newcant+' <i class="icon-plus2 pluscantcheckout" precio="'+ precio +'"></i><i class="icon-minus minuscantcheckout" precio="'+ precio +'"></i>');
		total_carrito -= precio;
		$('#totalcheckout').html('TOTAL: <span class="text-light">$'+addCommas(total_carrito)+'</span>');
	} else {
		alert('La cantidad mínima es 1');
	}
});

$(document).on('click', '.minuscant', function(){
	var cantdiv = $(this).parent();
	var cant = cantdiv.attr('value');
	if(cant > 1) {
		var newcant = cant - 1;
		cantdiv.attr('value', newcant);
		$('.agregarcart').attr('cant', newcant);
		cantdiv.html(newcant+' <i class="icon-plus2 pluscant"></i><i class="icon-minus minuscant"></i>');
	} else {
		alert('La cantidad mínima es 1');
	}
});

$(document).on('click', '.agregarcart', function(){
	var sku = $(this).attr('sku');
	var cant = $(this).attr('cant');
	var modelo = $(this).attr('modelo');
	$.ajax({
	    type:"GET",
	    url: page_url +  "cart.php",
	    data:"action=add&sku=" + sku + "&modelo=" + modelo + "&cantidad=" + cant,
	    success:function(e) {
	      	if(!$('#cart').hasClass('open')) {
	      		$('#cart').addClass('open');
				$('#carticon').addClass('icon-times');
				$('#carticon').removeClass('icon-cart');	
				$('#carticon').addClass('backred');
				$('#carticon').addClass('text-white');	
				$('#cart').slideToggle();
	      	}
	      	$('#cart').html(e);
	    }
	});
});


$(document).on('click', '.deleteproducto', function(){
	var ref = $(this).attr('ref');
	var minus = parseInt($(this).attr('minus'));
	$.ajax({
	    type:"GET",
	    url: page_url +  "cart.php",
	    data:"action=remove&ref=" + ref,
	    success:function(e) {
	      	location.reload();
	    }
	});
});

$(document).on('click', '.deletecart', function(){
	var ref = $(this).attr('ref');
	var minus = parseInt($(this).attr('minus'));
	var parent = $(this).parent().parent();
	var subtotal = 0;
	var total = 0;
	$.ajax({
	    type:"GET",
	    url: page_url +  "cart.php",
	    data:"action=remove&ref=" + ref,
	    success:function(e) {
	      	parent.fadeOut();
	      	subtotal = parseInt($('.totalcart').attr('value'));
	      	total = subtotal - minus;
	      	$('.totalcart').html('$'+addCommas(total));
	    }
	});
});

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

$(document).on('click', '.creardireccion', function(event){
	event.stopPropagation();
	if($('#creardireccion').hasClass('open')) {
		$('#creardireccion').removeClass('open');
	} else {
		$('#creardireccion').addClass('open');
	}
	$('#creardireccion').fadeToggle('slow');
});

$(document).on('click', '#closedirecciones', function(event){
	if($('#creardireccion').hasClass('open')) {
		$('#creardireccion').removeClass('open');
		$('#creardireccion').fadeToggle('slow');
	} 
});

$(document).on('click', '.direccion', function(event){
	var ciudad = $(this).attr('ciudad');
	var barrio = $(this).attr('barrio');
	var direccion = $(this).attr('direccion');
	$('inputciudad').val(ciudad);
	$('inputdireccion').val(direccion);
	$('inputbarrio').val(barrio);
	$('.direccion').removeClass('direccionsel');
	$(this).addClass('direccionsel');
});

$(document).on("keyup", "#search_input", function(event){
	event.preventDefault();
    var query = $("#search_input").val()
    if($("#search_input").val().length >= 2) {
	    $.ajax({
		    type:"GET",
		    url: page_url + "search.php",
		    data:"query=" + query,
		    success:function(e) {
		      	$("#resultadossearch").html(e)
		    }
		});
    }
    if($("#search_input").val().length < 2) {
    	$.ajax({
		    type:"GET",
		    url: page_url + "search.php",
		    data:"query=featured",
		    success:function(e) {
		      	$("#resultadossearch").html(e)
		    }
		});
    }
});

$(document).on('click', '#menuiconmobile', function(event){
	event.stopPropagation();
	if($('#menumob').hasClass('open')) {
		$('#menumob').removeClass('open');
		$('#menuiconmobile').addClass('icon-th-menu');
		$('#menuiconmobile').removeClass('icon-times');
	} else {
		$('#menuiconmobile').removeClass('icon-th-menu');
		$('#menuiconmobile').addClass('icon-times');
		$('#menumob').addClass('open');
	}
	$('#menumob').slideToggle('slow');
});








