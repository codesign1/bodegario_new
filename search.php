<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

## INCLUDE VENDOR
include 'config/configuration.php';
include 'vendor/autoload.php';

## USE NAMESAPCES
use Velocity\Velocity;
use Velocity\Authentication\Input;
use Velocity\Helpers\Helpers;
use Velocity\Ecommerce\Shop;

## EXECUTE CODE
$shop = new Shop();
if(Input::get('query')=='featured') {
	$products = $shop->get_productos_featured(8);
} else {
	$products = $shop->search_products(Input::get('query'));
}
?>

<div class="grid-12-xs last">

<?php for ($i=0; $i < count($products); $i++) { 
	$modelos = $shop->get_modelos_sku($products[$i]->sku); ?>

	<div  class="grid-3-m text-center producto <?php if($i % 4 == 3){ echo 'last'; } ?>">
	    <span class="icon-heart-outline favorito"></span>
	    <span class="icon-zoom-in-outline zoom"></span>
	    <span id="prod<?php echo $products[$i]->sku; ?>" class="precio addfast" model="<?php echo $modelos[0]->id; ?>" sku="<?php echo $products[$i]->sku; ?>">$<?php echo number_format($products[$i]->precio); ?> <span class="addfastcart icon-shopping-cart"></span> </span>
	    <div class="cont90">
	        <a href="/producto/<?php echo $products[$i]->url; ?>"><img id="img<?php echo $products[$i]->sku; ?>" src="/public/img/<?php echo $modelos[0]->img; ?>"></a>
	        <div class="text-center colorsdiv grid-12-xs last">
	        	<?php $j = 0; ?>
	        	<?php foreach ($modelos as $item) { ?>
	        		<?php if($item->color2 == 'None') { ?>
	        		    <span id="<?php echo $item->id; ?>" sku="<?php echo $item->sku; ?>" img="<?php echo $item->img; ?>" class="choosecolor color <?php if($j==0) { echo 'coloroutline'; } ?>" style="background-color:<?php echo $item->color1; ?>;"></span>
	        		<?php } else { ?>
	        		    <span id="<?php echo $item->id; ?>" img="<?php echo $item->img; ?>" sku="<?php echo $item->sku; ?>" class="choosecolor color <?php if($j==0) { echo 'coloroutline'; } ?>">
	        		    	<div class="grid50" style="background-color:<?php echo $item->color1; ?>;"></div>
	        		    	<div class="grid50" style="background-color:<?php echo $item->color2; ?>;"></div>
	        		    </span>
	        		<?php } ?> 
	        		<?php $j++; ?>
	        	<?php } ?>  
	        </div>
	        <a href="/producto/{{ prod.url }}" class="text-uppercase karla-bold"><?php echo $products[$i]->nombre; ?></a><br>
	        <a class="cate" href="/shop/<?php echo $products[$i]->categoria; ?>"><?php echo ucfirst(str_replace('-', ' ', $products[$i]->categoria)); ?></a> 
	        <span> | </span>
	        <a class="cate" href="/shop/<?php echo $products[$i]->categoria; ?>/<?php echo $products[$i]->sub_categoria; ?>"><?php echo ucfirst(str_replace('-', ' ', $products[$i]->sub_categoria)); ?></a>
	    </div>
	</div>

	<?php if($i % 4 == 3){ echo '</div><div class="grid-12-xs last">'; } ?>

<?php } ?>

</div>