<?php

session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

## INCLUDE VENDOR
include 'config/configuration.php';
include 'vendor/autoload.php';

## USE NAMESAPCES
use Velocity\Velocity;
use Velocity\Authentication\Input;
use Velocity\Helpers\Helpers;
use Velocity\Ecommerce\Shop;
use Velocity\Users\User;

## EXECUTE CODE
$user = new User();
if($user->isLoggedIn()) {
	$user_data = $user->data();
	$shop = new Shop($user_data->id);
} else {
	$shop = new Shop();
}

if(Input::get('action')=='add') {
	$sku = Input::get('sku');
	$modelo = Input::get('modelo');
	$cantidad = (Input::get('cantidad')) ? Input::get('cantidad') : 1;
	$shop->add_to_cart($sku, $modelo, $cantidad);
} elseif (Input::get('action')=='remove') {
	$ref = Input::get('ref');
	$shop->remove_from_cart($ref);
}

list($cart, $cart_total) = $shop->get_cart();
?>

<div class="grid-12-xs last">
	<div class="cont85">
		<div class="grid-7-xs">
			<span class="titulocart">TU ORDEN</span>
		</div>
		<div class="grid-5-xs text-right last">
			<span class="titulocart">PRECIO</span>
		</div>

		<?php if(count($cart)>0) { ?>

			<?php foreach ($cart as $cart_item) { ?>
			    
				<div class="grid-12-xs cartitem last">
					<div class="grid-1-xs">
						<i class="icon-times deletecart" minus="<?php $cart_item['cantidad'] * $cart_item['precio'] ?>" ref="<?php $cart_item['sku'].'||'.$cart_item['modelo']; ?>"></i>
					</div>
					<div class="grid-7-xs">
						<span class="text-uppercase itemdesc"><?php echo $cart_item['nombre']; ?> - Modelo <?php echo $cart_item['modelo']; ?> x <?php echo $cart_item['cantidad']; ?></span>
					</div>
					<div class="grid-4-xs text-right last">
						<span class="text-uppercase precio">$<?php echo number_format($cart_item['precio']); ?></span>
					</div>
				</div>

			<?php } ?>

			<div class="grid-12-xs text-right last">
				<span class="totalcart">TOTAL $<?php echo number_format($cart_total); ?></span>
			</div>
			<div class="grid-12-xs text-right last">
				<a href="/checkout" class="btncheckout">CHECKOUT</a>
			</div>

		<?php } else { ?>

			<p class="text-shadow karla-bold">Tu carrito esta vacio, agrega algo ya!</p>

		<?php } ?>
	</div>
</div>
